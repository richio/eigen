import os
from django.conf import settings
from django.shortcuts import render

from .models import Batch
from .utils import get_results, ProcessText


def home(request):
    return render(request, 'wordparser/home.html')


def show_results(request):
    """
    Process one or more documents and display results
    in a table.
    """
    # create a new batch
    batch = Batch.objects.create()

    # get list of uploaded files
    doc_directory = os.path.join(settings.BASE_DIR, 'documents/eigen')
    files = os.listdir(doc_directory)

    # process each document and add to database
    for f in files:
        ProcessText(os.path.join(doc_directory, f), batch)

    results = get_results(sentence_limit=3, **{'batch_id': batch.id})

    return render(request, 'wordparser/results.html', {
        'results': results, 'batch_id': batch.id
    })


def word_detail(request, batch_id, word):
    """
    Display a word with all sentences it appears in.
    """
    results = get_results(**{'batch_id': batch_id, 'word': word})
    context = {'results': results, 'word': word}
    return render(request, 'wordparser/word_detail.html', context)
