from django.apps import AppConfig


class WordparserConfig(AppConfig):
    name = 'wordparser'
