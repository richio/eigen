from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('results/', views.show_results, name='results'),
    path('results/<batch_id>/<word>/', views.word_detail, name='word-detail')
]
