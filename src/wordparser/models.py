from django.db import models

from core.models import TimeStampedModel


class Batch(models.Model):
    """
    Denotes a set of one of more ``Documents`` uploaded
    at the same time.
    """
    created = models.DateTimeField(auto_now_add=True)


class Document(TimeStampedModel):
    """
    Refers to a text
    """
    title = models.CharField(max_length=255)
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Sentence(TimeStampedModel):
    sentence = models.TextField()
    document = models.ForeignKey(Document, on_delete=models.CASCADE)

    def __str__(self):
        return self.sentence


class Word(TimeStampedModel):
    word = models.CharField(max_length=128)
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
    sentence = models.ForeignKey(Sentence, on_delete=models.CASCADE)
    # sentences = models.ManyToManyField(Sentence, through='References')

    class Meta:
        ordering = ['word']

    def __str__(self):
        return self.word


# class References(models.Model):
#     word = models.ForeignKey(Word, on_delete=models.CASCADE)
#     sentence = models.ForeignKey(Sentence, on_delete=models.CASCADE)
