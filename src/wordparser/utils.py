from os.path import basename
from string import punctuation

from django.db.models import Count

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
# from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import sent_tokenize, word_tokenize

from .models import Document, Sentence, Word


class ProcessText:
    """
    Read a .txt file and use the Natural Language Processing
    package ``nltk`` to save all interesting words to
    Django models.
    """
    # add some extra common punctuation to string.punctuation
    punct = list(punctuation)
    punct.extend(['``', '--', "''"])

    # add a few extra values to stop words based on results
    stop_words = stopwords.words('english')
    stop_words.extend(["'s", "'ve", "n't", "'re", "'d", "'ll", "’"])

    def __init__(self, filepath, batch):
        """
        :param filepath: str, relative path to file
        :param batch: ``Batch`` object
        """
        self.word_objects = []
        self.filepath = filepath
        self.batch = batch
        self.doc = self.create_document()
        self.text = self.read_file()
        self.sentences = self.tokenise_sentences()
        for self.sent in self.sentences:
            self.sent_obj = self.create_sentence(self.sent)
            self.tokens = self.tokenise_words(self.sent)
            self.filtered = self.remove_stop_words_and_punct()
            self.stemmed = self.stem_words()
            self.word_objects.extend(self.generate_list_of_word_objects())
        self.bulk_create_words()

    def read_file(self):
        """
        Open a .txt file at given filepath and
        read to memory
        :return: str
        """
        with open(self.filepath) as text_file:
            return text_file.read()

    def create_document(self):
        """
        Create ``Document`` instance
        :return: ``Document`` instance
        """
        title = basename(self.filepath)
        return Document.objects.create(title=title, batch=self.batch)

    def tokenise_sentences(self):
        """
        Use nltk ``sent_tokenize`` to split text string
        into sentences
        :return: list of sentence strings
        """
        return sent_tokenize(self.text)

    def create_sentence(self, sent):
        """
        Save the sentence to the ``Sentence`` model
        :param sent: str, sentence
        :return: ``Sentence`` object
        """
        return Sentence.objects.create(sentence=sent, document=self.doc)

    @staticmethod
    def tokenise_words(sent):
        """
        Use nltk ``word_tokenize`` to split a sentence
        into a list of words
        :param sent: str
        :return: list of word strings
        """
        return word_tokenize(sent)

    def remove_stop_words_and_punct(self):
        """
        Remove common words and punctuation from a sentence
        :return: list of interesting words
        """
        filtered = [
            w for w in self.tokens if w.lower() not in
            self.stop_words and w not in self.punct
        ]
        return filtered

    def stem_words(self):
        """
        Reduce words to their 'stemmed' form
        :return: list of stemmed words
        """
        ps = PorterStemmer()
        filtered = [ps.stem(w) for w in self.filtered]
        return filtered

    # Not using this due to significant performance impact
    # def lemmatize(self):
    #     lem = WordNetLemmatizer()
    #     return [lem.lemmatize(w) for w in self.words]

    def generate_list_of_word_objects(self):
        """
        In order to perform a bulk CREATE statement, use
        a generator to make a list of yet uncommited
        ``Word`` objects
        :return: list of ``Word`` create expressions
        """
        word_generator = (
            Word(
                word=w,
                sentence=self.sent_obj,
                batch=self.batch
            ) for w in self.stemmed
        )
        return list(word_generator)

    def bulk_create_words(self):
        """
        Use Django's ``bulk_create`` to run a bulk SQL
        CREATE statement.
        :return: ``Word`` objects (no id)
        """
        return Word.objects.bulk_create(self.word_objects)


def get_results(sentence_limit=None, **kwargs):
    """
    For a given set of parameters, return a list of Words
    and the sentence and documents they appear in.
    :param sentence_limit: optional arg (int) for queryset slicing
    :return: list of lists [word, wordcount, docs, sentences]
    """
    results = []

    words = Word.objects.filter(**kwargs).values('word')
    # limit to 100 words to save processing time
    words = words.annotate(Count('word')).order_by('-word__count')[:100]

    for word in words:
        sentences = Word.objects.filter(word=word['word'], batch_id=kwargs['batch_id'])
        sentences = sentences.select_related(
            'sentence',
            'sentence__document',
            'sentence__document__title'
        ).values(
            'sentence__sentence',
            'sentence__document__title'
        )
        docs = set([d['sentence__document__title'] for d in sentences])
        if not sentence_limit:
            sents = [s['sentence__sentence'] for s in sentences]
        else:  # limit only 3 sample sentences for results page
            sents = [s['sentence__sentence'] for s in sentences][:3]

        results.append([word['word'], word['word__count'], docs, sents])

    return results
