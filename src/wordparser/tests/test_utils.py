from django.test import TestCase

from ..models import Batch
from ..utils import ProcessText


class ProcessTextTests(TestCase):
    """
    Test the ``ProcessText`` util with a small test doc
    containing 3 sentences (see documents/testdir/testdoc.txt)
    """

    @classmethod
    def setUpTestData(cls):
        cls.batch = Batch.objects.create()
        cls.inst = ProcessText('documents/testdir/testdoc.txt', cls.batch)
        cls.doc = cls.inst.create_document()

    def test_create_document_title(self):
        """
        ``create_document`` should create a ``Document``
        instance with the title 'testdoc.txt'
        """
        self.assertEqual(self.doc.title, 'testdoc.txt')

    def test_read_file_returns_string(self):
        """
        ``read_file`` should return a string
        """
        self.text = self.inst.read_file()
        self.assertIsInstance(self.text, str)

    def test_tokenise_sentences_returns_known_list_len(self):
        """
        ``tokenise_sentences`` should find 3 sentences in
        'testdoc.txt'
        """
        sentences = self.inst.tokenise_sentences()
        self.assertEqual(len(sentences), 3)

    def test_sentence_foreign_keys(self):
        """
        Three ``Sentences`` should have been created with
        ``self.doc`` as the FK.
        """
        sentences = self.inst.doc.sentence_set
        self.assertEqual(sentences.count(), 3)

    def test_punctuation_removed(self):
        """
        Words passed through ``remove_stop_words_and_punct``
        should not contain any punctuation listed in ``punct``
        """
        for word in self.inst.filtered:
            self.assertTrue(word not in self.inst.punct)

    def test_stopwords_removed(self):
        """
        Words passed through ``remove_stop_words_and_punct``
        should not contain any stopwords listed in ``stop_words``
        """
        for word in self.inst.filtered:
            self.assertTrue(word not in self.inst.stop_words)

    def test_correct_amount_of_words_in_generator(self):
        """
        ``generate_list_of_word_objects`` should generate a
        list of words the same length as ``stemmed``
        """
        self.assertEqual(
            len(self.inst.stemmed),
            len(self.inst.generate_list_of_word_objects())
        )

    def test_bulk_create_words_created_correct_amount(self):
        """
        ``generate_list_of_word_objects`` should create
        the same amount of instances as in ``word_objects``
        """
        self.assertEqual(
            len(self.inst.word_objects),
            len(self.inst.bulk_create_words())
        )

